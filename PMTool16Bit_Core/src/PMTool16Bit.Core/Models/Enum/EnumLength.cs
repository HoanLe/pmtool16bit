﻿namespace PMTool16Bit.Models.Enum
{
    public static class EntityLength
    {
        public const int ProjectName = 256;
        public const int GroupTaskName = 256;
        public const int TaskName = 256;
        public const int Description = 500;
        public const int ShortDescription = 256;
        public const int CommentContent = 500;
        public const int ProjectRole = 100;
        public const int IdLimit = 128;
        public const int FileName = 256;
        public const int FileType = 256;
        public const int FilePath = 500;
    }
}