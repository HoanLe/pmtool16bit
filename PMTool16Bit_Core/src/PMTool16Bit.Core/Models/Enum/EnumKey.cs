﻿namespace PMTool16Bit.Models.Enum
{
    public static class SettingKey
    {
        public const string App_BaseUrl = "App_BaseUrl";
        public const string App_FrontEndBaseUrl = "App_FrontEndBaseUrl";
    }
    public static class ProjectType
    {
        public const string Owner = "owner";
        public const string All = "all";
        public const string Participated = "participated";

    }
}