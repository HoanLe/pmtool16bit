﻿using System.ComponentModel.DataAnnotations;

namespace PMTool16Bit.Users
{
    public class ChangePasswordDto
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}