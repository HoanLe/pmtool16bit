﻿using Abp.AutoMapper;
using PMTool16Bit.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PMTool16Bit.Services
{
    [AutoMapFrom(typeof(EventTask))]
    public class EventTaskOrderDto : BaseDto
    {
        [Range(0, 20)]
        public int TaskOrder { get; set; }
    }
}