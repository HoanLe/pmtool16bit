﻿namespace PMTool16Bit.Services
{
    public class TaskGroupDropdownDto
    {
        public int Id { get; set; }

        public string TaskGroupName { get; set; }
    }
}